# README #

This is a repository for a book on basic programming terminology and constructs.  The code in this repo is compiled into a book on [LeanPub](http://www.leanpub.org) after each commit.  If you would like to help write the book thwn make a fork, do some editing, and submit a pull request!  As a general rule, all pull requests will be honored as long as they fit the guidelines.  Please join us!

Here is the link to the book page:
[Developer's Little Black Book](https://leanpub.com/developerslittleblackbook)

### What is this repository for? ###

* Creating a simple and concise book on basic programming skills
* Version 0.1

### How do I get set up? ###

* Fork the repo at [BitBucket](https://drnordstedt@bitbucket.org/drnordstedt/programmerslittleblackbook.git)
* Edit
* Submit Pull Request

### Contribution guidelines ###

* Keep each topic to one page (ideal) or two pages max)
* Content should be professional and free from profanity and potentially offensive material
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
