# Introduction

This is a quick reference guide for developers, software engineers, and architects to refresh their memories on some of those seemingly endless technical items we're all expected to remember.  The trouble is that if you aren't using something fairly often, the details fall out of your working memory.  Google is an excellent reference, but may take some time to find a simple example and you're likely to end up getting lost in a series of blog posts and going down the perpetual rabbit hole.   This guide is meant to serve as a simple refresher for those tasks you've done before and need a fast reminder.

This book can be used in your day-to-day job and design meetings as well as giving to an interviewee to use while asking those interview programming questions to solve problems on the whiteboard.  My feeling is we shouldn't be testing someone's memory during an interview, but rather their capability to solve problems with others on the team.

Here's a brief rundown of the book's contents:

|                   |                   |               |
|:-------------------|:-------------------:|---------------:|
| Variables         | Operators         | Constructs    |
| Lists     | Hashmaps     | Sorting    |
| TCP/IP       | UDP     | TCP    |
| HTTP     | REST     | JSON    |
| Design Patterns     | Recursion     | Recursion      |
| Linear Regression       | Statistics     | SQL    |
| Calculus Derivatives       | Discrete Mathematics      | Boolean Algebra      |
| Calculus Integration       | Lambda Calculus       | Database Design      |
| Linear Algebra       | Message Queues       | CQRS      |
| Functional Composition       | Event Store       | LINQ    |
