# Chapter Two: Principles and Theorems

{pagebreak}

## The CAP Theorom

Consistency, Availability, and Partitioning.
You can only have two out of three.

{pagebreak}

## SOLID

In Object Oriented Programming, these five principles have been established to help ensure the creation of modular and maintable code.

* Single Responsibility
* Open-Closed
* Liskov Substitution
* Interface Segregation
* Dependency Inversion

{pagebreak}

## DRY

Don't Repeat Yourself

{pagebreak}

## ACID

* Atomicity
* Consistency
* Isolation
* Durability

## BASE

* Basic Availability
* Soft State
* Eventually Consistent

[ACID vs BASE Explained on Neo4J Blog](http://neo4j.com/blog/acid-vs-base-consistency-models-explained/)
