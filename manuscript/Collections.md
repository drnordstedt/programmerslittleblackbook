# Collections

Collections are a number of objects collected together and the mappings/functions applied on them.  For example, we may have a list of integers such as the number of items shipped per day over a month.  We may have a list of strings that identify all the people working in an office.  We may have a mapping of each office worker and the number of items they individually shipped on a certain day.  And that mapping could be in a list that represents each day in the month!

{pagebreak}

## Arrays

Arrays are on of the most fundamental collection types.  A basic array is a list of fixed sized that contains one type of element.

{pagebreak}

## Lists

Lists are very much like arrays, but are expanded with a backing library to support adding, deleting, selecting, and filtering.

{pagebreak}

## Maps

A Map is basically a list of mappings from one object to another.  Each item in the list is a pair, with the first object being the key and the second object being the value.

{pagebreak}

## HashMaps

A HashMap is the same as a Map, but with the key elements being accessed more efficiently to later retrieval.  A Map will typically have better performance adding to the collection, whereas a Hashmap will have better performance retrieving the values from the collection.

{pagebreak}

