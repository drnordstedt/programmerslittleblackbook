# Enterprise Integration Patterns

{pagebreak}

## Message

(http://www.enterpriseintegrationpatterns.com/patterns/messaging/Message.html)

{pagebreak}

## Event Message

(http://www.enterpriseintegrationpatterns.com/patterns/messaging/EventMessage.html)

(pagebreak)

## Message Channel

(http://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageChannel.html)

{pagebreak}

## Channel Adapter

(http://www.enterpriseintegrationpatterns.com/patterns/messaging/ChannelAdapter.html)

{pagebreak}

## Dead Letter Channel

(http://www.enterpriseintegrationpatterns.com/patterns/messaging/DeadLetterChannel.html)

{pagebreak}

## Wire Tap

(http://www.enterpriseintegrationpatterns.com/patterns/messaging/WireTap.html)

{pagebreak}
