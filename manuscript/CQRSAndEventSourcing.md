# CQRS & Event Sourcing

Command Query Responsibility Segregation is  a pattern that says very simply to keep your data commands and queries separate from one another.  This goes so far as to say there there is a separate database for writing that there is for reading.  What this buys you is faster writes and faster reads because neither side is locking up the database for the other.  The cost for this is going to be a more complex design.

Event sourcing works extremely well in the CQRS space.  As events are processed, the data is written.

This system is good because of the separation of concerns and simplicity of processing events and data.

{pagebreak}

## CQRS

One item to note when doing CQRS is the fact that there will be a delay from when the data is written to to the time that it is available for reading.  The technical term for this is *Eventual Consistency* and it is fine; just something to be aware of.

Typically there will be one database for writing and another for reading with another process in the middle that syncs the data between the two databases.

### Write


### Read


{pagebreak}

## Event Sourcing

As a good reference, refer to Greg Young's work at
[https://geteventstore.com/](https://geteventstore.com/)

{pagebreak}

## CQRS with Event Sourcing

CQRS can be implemented using Event Sourcing and the two technologies fit very well together!  The way it works is that each event coming in is processed with the appropriate data being written to the Write database.  You will still need to implement the sync logic between the Read and Write databases, but the event system can be an off the shelf system such as MSMQ or RabbitMQ.

<insert diagram here>
