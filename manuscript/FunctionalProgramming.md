# Functional Programming

Functional programming has been around for a long, long time and has recently (~2010) gained much strength in popularity.  The older languages include Lisp, Scheme, Erlang, and Haskell.  Newer variations are Scala, Clojure, and F#.   

Function programming relies heavily on...wait for it...functions!  What did you expect, anyway?  Seriously, though, the languages incorporate functions as a first class citizen.  What does that mean?  Well, functions are used as any other variable/object and can be passed around to other functions and used to compose other functions.

{pagebreak}

## Functions as First Class Citizens

{pagebreak}

## Pattern Matching

{pagebreak}

## Case Classes


