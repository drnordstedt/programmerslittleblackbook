# Algorithms

What are algorithms?  Algorithms are basically a plan to solve a particular problem.  A series of steps that can be reproduced time after time to create a solution.   You may even call them solution patterns.  

What we will look at here is some of the more common algorithms you may be expected to know as a basic or intermediate developer.

{pagebreak}

## Bubble Sort

{pagebreak}

## Merge Sort

{pagebreak}

## Depth First Search

{pagebreak}

## Breadth First Search

{pagebreak}

## N Queens


