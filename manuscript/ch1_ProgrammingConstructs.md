# Chapter One: Programming Constructs

## Do While Loop

A do-while loop is used when a block of code should be executed at least once.  If the condition at the end  of the construct evaluates to true, then execution will begin at the top of the loop.  This code execution will continue until the while condition evaluates to false.

~~~~~~~~
do {
  // do something at least once
} while (condition is true)
~~~~~~~~

**Example:**

~~~~~~~~
int counter = 0;
do {
  // Notify the listener 10 times
  NotifyListener();
  counter = counter + 1;
} while (counter < 10)
~~~~~~~~

{pagebreak}

## While Do Loop

A while loop is used when you want to evaluate a condition **before** executing the loop and again each time the executions starts again at the top of the loop.

~~~~~~~~
while (condition is true)  {
  // do something
}
~~~~~~~~

**Example:**

~~~~~~~~
int counter = 0;
while (counter < 10)  {
  NotifyListener();
  counter = counter + 1;
}
~~~~~~~~

{pagebreak}

## For Loop

A for loop is typically used when iterating over a sequential data structure such as a list or array.  There are four parts to the for loop structure: the initializer, the predicate, the execution body, and the post-loop action (usually an increment of the for loop variable).

The code sample below creates an integer array of size four, then iterates over each integer and outputs the value to stdout.

~~~~~~~~
int[] myInts = int[] { 1,3,5,7 };
for (int i=0; i < myInts.Length; i = i + 1)  {
  println(myInts[i]);
}
~~~~~~~~

{pagebreak}


## ForEach Loop

A foreach loop is a higher-level construct and does not use the index numer of an item in a data structure, but simply uses each value in the data structure as the execution block is executed.

The code sample below creates an integer array of size four, then iterates over each integer and outputs the value to stdout.  This has the same functionality as the for loop example on the previous page, but the code size has been reduces and the code readability improved.

~~~~~~~~
int[] myInts = int[] { 1,3,5,7 };
foreach (int myInt in myInts)  {
  println(myInt);
}
~~~~~~~~
