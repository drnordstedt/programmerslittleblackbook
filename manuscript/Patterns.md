# Patterns

Creational, Behavioral, and Structural

{pagebreak}

## Template

{pagebreak}

## Factory

{pagebreak}

## Abstract Factory

The Abstract Factory is used to create objects without knowing the actual implementation of the creator.  This creator is defined either based on implicit detail such as run-time DI or based on information passed in to the factory.

{pagebreak}

## Factory Method

{pagebreak}

## Strategy

{pagebreak}

## Singleton

{pagebreak}

## Composite

{pagebreak}

## Builder

{pagebreak}
## Chain of Responsibility

{pagebreak}

## Command

{pagebreak}

## Observer

{pagebreak}

## Null Object

{pagebreak}

## Retry

A retry pattern is used when attempting to perform an operation that has the possibility of failure on occasion.  This occurs most often when accessing external resources such as a database, filesystem, or network service like an HTTP REST service.

{pagebreak}

## Circuit Breaker

A circuit breaker pattern is used in combination with the Retry Pattern to stop the retry attempts at some given point.

{pagebreak}

