# Imperative Programming

Imperative programming has been pretty much the staple of programming life for at least the past thirty years.  In a nutshell, imperative programming changes a program's state at runtime.  This differs dramatically from a functional programming style that aspires to no mutable state anywhere.

A comparison can be found at [https://msdn.microsoft.com/en-us/library/bb669144.aspx](https://msdn.microsoft.com/en-us/library/bb669144.aspx)
