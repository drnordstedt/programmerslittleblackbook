# Actors

The Actor Model was created by Carl Hewitt in a paper written in 1973, 'A Universal Modular Actor Formalism for Artificial Intelligence'

In the original definition, an Actor is defined to do three things.  A fourth one has been added as we feel it is important to state the fact of changing state to respond to new messages.

* Receive messages
* Send messages to other actors
* Create new Actors
* Change state to respond to new messages
