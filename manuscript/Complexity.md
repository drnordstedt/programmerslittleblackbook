# Complexity

{pagebreak}

## Big-O Notation

I went through many exercises and exams in college to make sure I knew 'Big O'.  It was somewhat painful, albeit persistent memory.  Big-O is good stuff to know, but something not usually asked about except during interviews.

Basically, you should know the basics, but don't dwell on the details.


